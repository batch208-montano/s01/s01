package com.b208.s1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Array;
import java.util.ArrayList;

@SpringBootApplication
@RestController
//It will tell springboot that this application will function as an endpoint handling web requests.
public class S1Application {
	//Annotations in Java Springboot marks classes for their intended functionalities.
	//Springboot upon startup scans for classes and assign behaviors based on their annotation.

	public static void main(String[] args) {
		SpringApplication.run(S1Application.class, args);}

	//@GetMapping will map a route or an endpoint to access to run our method.
	//When http://localhost:8080/hello is accessed from a client, we will be able to run the hi() method.
	@GetMapping("/hello")
	public String hello(){
		return "Hi from our Java Springboot App!";
		}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue = "John")String nameParameter){
		//Pass data through the url using our string query
		//http://localhost:8080/hi?name=Joe
		//We retrieve the value of the filed "name" from our URL
		//defaultValues is the fallback value when the query string or request param is empty.
		//System.out.println(nameParameter);
		return "Hi! My name is " + nameParameter;
	}

	//Create a new method called myFavoriteFood()
	//Map an endpoint to the method using @GetMapping()
		//Method receives data as RequestParam and value is food.
		//Add an appropriate String parameter.
		//return the message to the client:
	//"Hi! My favorite food is <value of food>"

	@GetMapping("/favfood")
	public String favfood(@RequestParam(value="food", defaultValue = "Fried Chicken")String foodNameParams) {

		return "Hi! My favorite food is " + foodNameParams;
	}
	//2 ways of passing data through the URL by using Java Springboot.
	//Query String using Request Params - Directly passing data into the url and getting the data.
	//Path Variable is much more similar to ExpressJS's req.params

	@GetMapping("/greeting/{name}")
	public String greeting(@PathVariable("name")String nameParams){
		//@PathVariable() allows us to extract data directly from the url.
		return "Hello " + nameParams;
	}

	//1. Create a New ArrayList for Strings called enrollees property in the S1Application class.
	ArrayList<String> enrollees = new ArrayList<>();

	//2. Create a /enroll route that will accept a query string with the parameter of user which:
	// a. Adds the user's name in the enrollees array list.
	// b. Returns a welcome(user)! Message.

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="user", defaultValue = "John")String enrolleeNameParameter){
		return "Welcome " + enrolleeNameParameter+"!";
	}

	//3. Create a new /getEnrollees route which will return the enrollees ArrayList.
	@GetMapping("/getEnrollees")
	public ArrayList<String> enrolleesArr(){
		enrollees.add("Jeff");
		enrollees.add("James");
		return enrollees;
	}

	//4. Create /courses/{id} dynamic route using a path variable of id that:

	//a. If the path variable passed is "java101" return the course name, schedule and price.

	//b. If the path variable passed is "sql101" return the course name, schedule and price.

	//c. If the path variable passed is "jsoop101" return the course name, schedule and price.

	//D. Else return a message that the course cannot be found.

	@GetMapping("/courses/{id}")
	public String courseLists(@PathVariable("id")String courseNameParams) {
		//@PathVariable() allows us to extract data directly from the url.

		if(courseNameParams.equals("java101")){
			return "Name: " + courseNameParams + " Schedule: MWF 8:00 AM - 11:00AM, " + "Price: PHP3000";
		} else if(courseNameParams.equals("sql101")){
			return "Name: " + courseNameParams + " Schedule: TTH 8:00 AM - 11:00AM, " + "Price: PHP4000";
		} else if(courseNameParams.equals("jsoop101")){
			return "Name: " + courseNameParams + " Schedule: MW 1:00 PM - 4:00PM, " + "Price: PHP3500";

		} else {
			return "Course cannot be found";
		}

	}














}
